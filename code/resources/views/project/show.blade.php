<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-full	 mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                   <h2>{{ __("List projects") }}</h2>
                </div>
                <div class="p-3 text-gray-900">
                    <a href="{{ route("project.create") }}" >New record</a>
                    <table class="curdtable">
                        <thead>
                            <tr>
                                <th>
                                    id
                                </th>
                                <th>
                                    name
                                </th>
                                <th>
                                    event
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($projects as $project)
                                <tr>
                                    <td>
                                        {{$project->id}}
                                    </td>
                                    <td>
                                        {{$project->name}}
                                    </td>
                                    <td>
                                        <x-tableevent
                                                :nameroute="route('project.edit',['id'=>$project->id])"
                                                :nameroutedelet="route('project.destroy',['id'=>$project->id])"
                                        >
                                        </x-tableevent>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
