<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ApiProjectController extends Controller
{
    public function listdata(Request $request){
        $datas=Project::all();
        return response()->json($datas,202);
    }
}
