<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request){
        //    dd($request->only(['email','password']));
        if(!Auth::attempt($request->only(['email','password'])))
        {
            throw new AuthenticationException();
        }
        $user = User::where("email",$request->email)->first();
        $token=$user->createToken('Api token'. $user->name)->plainTextToken;
        return response()->json([
            'status' => true,
            'message' => 'User Logged In Successfully',
            'access' => $token
        ], 200);
    }

    public function logout(Request $request){

    }
}
