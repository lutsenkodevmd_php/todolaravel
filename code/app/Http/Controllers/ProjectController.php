<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProjectController extends Controller
{
    /**
     * Display all project in database
     * @return View
     */
    public function show(): View
    {
        $projects=Project::all();
        return view('project.show',
            ['projects'=>$projects]
        );
    }
    /**
     * Display form edit project
     * @return View
     */
    public function edit($id): View
    {
        $project=Project::find($id);
        return view('project.edit',
            ['project'=>$project]
        );
    }
    /**
     * Display form create new project
     * @return View
     */
    public function create(): View
    {
        return view('project.create');
    }


    /**
     * Update date project
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,$id){
        DB::table("projects")
            ->where("id",$id)
            ->update(
                ['name'=>$request->input('name')]
            );
        return redirect()->back();
    }

    /**
     * New record in database
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $newrecord=new Project();
        $newrecord->name=$request->input('name');
        $newrecord->save();
        return redirect()->route("project.show");
    }

    /**
     * Delete record
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Project::where("id",$id)->delete();
        return redirect()->route("project.show");
    }
}
