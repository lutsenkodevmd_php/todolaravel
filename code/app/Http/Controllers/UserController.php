<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

/**
 *
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Display all project in database
     * @return View
     */
    public function show(): View
    {
        $users=User::all();
        return view('user.show',
            ['users'=>$users]
        );
    }
    /**
     * Display form edit
     * @return View
     */
    public function edit($id): View
    {
        $user=User::find($id);
        return view('user.edit',
            ['user'=>$user]
        );
    }


    /**
     * Display form new create
     * @return View
     */
    public function create(): View
    {
        return view('user.create');
    }

    /**
     * Update data user (ID)
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,$id){
        DB::table("users")
            ->where("id",$id)
            ->update(
                ['name'=>$request->input('name'),
                 'email'=>$request->input('email')
                    ]
            );
        return redirect()->back();
    }


    /**
     * Set new pasword user
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatepasswprd(Request $request,$id){
        $password=Hash::make($request->input('password'));
        DB::table("users")
            ->where("id",$id)
            ->update(
                ['password'=>$password]
            );
        return redirect()->back();
    }

    /**
     * Create in database new record
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $password=Hash::make($request->input('password'));
        $newuser=new User();
        $newuser->name=$request->input('name');
        $newuser->email=$request->input('email');
        $newuser->password=$password;
        $newuser->save();
        return redirect()->route("user.show");
    }

    /**
     * Delete record user
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        User::where("id",$id)->delete();
        return redirect()->route("user.show");
    }

}
